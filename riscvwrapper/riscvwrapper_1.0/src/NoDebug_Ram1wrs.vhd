----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 19.11.2019 15:10:19
-- Design Name: 
-- Module Name: Ram_1wrs - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.math_real.log2;
use IEEE.Numeric_Std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity NoDebug_Ram_1wrs is
    generic (
        wordCount : integer := 1024;
        wordWidth : integer := 32;
        maskWidth : integer := 4;
        maskEnable : integer := 1;
        readUnderWrite : STRING := "dontCare";
        technology : STRING := "auto";
        userLabel : STRING := ""
    ); 
    Port ( clk : in STD_LOGIC;
           en : in STD_LOGIC;
           wr : in STD_LOGIC;
           mask : in std_logic_vector(maskWidth-1 downto 0);
           addr : in STD_LOGIC_vector(integer(log2(real(wordCount)))-1 downto 0);
           wrData : in STD_LOGIC_vector(wordWidth-1 downto 0);
           rdData : out STD_LOGIC_vector(wordWidth-1 downto 0)
    );
end NoDebug_Ram_1wrs;

architecture Behavioral of NoDebug_Ram_1wrs is
 type ram_type is array (0 to wordCount-1) of std_logic_vector(rdData'range);
  signal ram : ram_type;
  signal read_address : std_logic_vector(addr'range);
  signal we_masked : std_logic_vector(maskWidth-1 downto 0);
  
component mem_128k is
    Port ( 
      clka : in STD_LOGIC;
      ena : in STD_LOGIC;
      wea : in STD_LOGIC_VECTOR ( 3 downto 0 );
      addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
      dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
      douta : out STD_LOGIC_VECTOR ( 31 downto 0 )
    );
end component;  

component mem_128k_rom is
    Port ( 
      clka : in STD_LOGIC;
      ena : in STD_LOGIC;
      wea : in STD_LOGIC_VECTOR ( 3 downto 0 );
      addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
      dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
      douta : out STD_LOGIC_VECTOR ( 31 downto 0 )
    );
end component;  

  
component ram_4k_secure_data is
  Port ( 
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 3 downto 0 );
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end component;  

component ram_2k_secure_data is
  Port ( 
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 3 downto 0 );
    addra : in STD_LOGIC_VECTOR ( 8 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end component;  
  
begin
    gen_we_mask: for I in 0 to maskWidth-1 generate
        we_masked(I) <= mask(I) and wr when maskEnable = 1 else wr;
    end generate;
    
gen_128k: if wordCount = 32768 generate
    gen_rom: if userLabel = "rom" generate
        rv_ram_128k: mem_128k_rom port map (
            clka => clk,
            ena => en,
            wea => we_masked,
            addra => addr,
            dina => wrData,
            douta => rdData
        );
    end generate;
    gen_ram: if userLabel = "ram" generate
        rv_ram_128k: mem_128k port map (
            clka => clk,
            ena => en,
            wea => we_masked,
            addra => addr,
            dina => wrData,
            douta => rdData
        );
    end generate;
end generate;

gen_4k: if wordCount = 1024 generate
    gen_4k_secure: if userLabel = "secure" generate
        rv_ram_4k: ram_4k_secure_data port map (
        clka => clk,
        ena => en,
        wea => we_masked,
        addra => addr,
        dina => wrData,
        douta => rdData
    );
    end generate;
end generate;

gen_2k: if wordCount = 512 generate
    gen_2k_secure: if userLabel = "secure" generate
        rv_ram_2k: ram_2k_secure_data port map (
        clka => clk,
        ena => en,
        wea => we_masked,
        addra => addr,
        dina => wrData,
        douta => rdData
    );
    end generate;
end generate;
end Behavioral;
