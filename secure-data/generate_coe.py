#!/bin/python
import os
import configparser

config = configparser.ConfigParser()
config.read("keydata.txt")

apikey = config["keys"]["apikey"]
secretkey = config["keys"]["secretkey"]


def gen32BitArray(key):
    if len(key) % 8 != 0:
        raise("error")

    ret = []
    for i in range(0, len(key) / 8):
        val = int(key[i*8:i*8+8], 16)
        val_reversed = (val & 0xff000000) >> 24 | (val & 0x00ff0000) >> 8 | (val & 0x0000ff00) << 8 | (val & 0x000000ff) << 24
        hexstr = hex(val_reversed)[2:]
        while len(hexstr) < 8:
            hexstr = "0"+hexstr
        ret.append(hexstr)
    return ret
        
fw = open("data.coe", "w")


data = []
data.extend(gen32BitArray(apikey))
data.extend(gen32BitArray(secretkey))

fw.write("memory_initialization_radix=16;\n")
fw.write("memory_initialization_vector=\n")


print(str(data))
for i in range(0, len(data)):
    fw.write(data[i])
    if i == len(data)-1:
        fw.write(";\n")
    else:
        fw.write(",\n")

fw.close()
