set ppath [ get_property DIRECTORY [current_project] ]
puts $ppath
set bdfile "$ppath/iccfpga.srcs/sources_1/bd/design_iccfpga/design_iccfpga.bd"
puts ""
puts "bdfile:   $bdfile"
puts ""
update_compile_order -fileset sources_1
reset_run synth_1
reset_target all [get_files  $bdfile]
export_ip_user_files -of_objects  [get_files  $bdfile] -sync -no_script -force -quiet

