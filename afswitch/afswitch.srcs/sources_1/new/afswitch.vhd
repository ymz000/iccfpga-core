 -- IOTA Crypto Core
 --
 -- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
 -- discord: pmaxuw#8292
 -- https://gitlab.com/iccfpga-rv
 --
 -- Permission is hereby granted, free of charge, to any person obtaining
 -- a copy of this software and associated documentation files (the
 -- "Software"), to deal in the Software without restriction, including
 -- without limitation the rights to use, copy, modify, merge, publish,
 -- distribute, sublicense, and/or sell copies of the Software, and to
 -- permit persons to whom the Software is furnished to do so, subject to
 -- the following conditions:
 --
 -- The above copyright notice and this permission notice shall be
 -- included in all copies or substantial portions of the Software.
 --
 -- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 -- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 -- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 -- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 -- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 -- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 -- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity afswitch is
    generic (
        NUM_IOS : integer := 21
    );
    Port ( 
-- SPI0        
        spi0_io0_i : out std_logic;
        spi0_io0_o : in std_logic;
        spi0_io0_t : in std_logic;

        spi0_io1_i : out std_logic;
        spi0_io1_o : in std_logic;
        spi0_io1_t : in std_logic;

        spi0_sck_i : out std_logic;
        spi0_sck_o : in std_logic;
        spi0_sck_t : in std_logic;
        
        spi0_ss_i : out std_logic_vector(1 downto 0);
        spi0_ss_o : in std_logic_vector(1 downto 0);
        spi0_ss_t : in std_logic;
-- I2C0
        i2c0_scl_i : out std_logic;
        i2c0_scl_o : in std_logic;
        i2c0_scl_t : in std_logic;
        
        i2c0_sda_i : out std_logic;
        i2c0_sda_o : in std_logic;
        i2c0_sda_t : in std_logic;
-- UART0
        rx : out std_logic;
        tx : in std_logic;
-- GPIO-I/O
        gpio_io_i : out std_logic_vector(31 downto 0);
        gpio_io_o : in std_logic_vector(31 downto 0);
        gpio_io_t : in std_logic_vector(31 downto 0);
-- AF-select
        af_o : in std_logic_vector(31 downto 0);
        
-- I/Os
        io : inout std_logic_vector(NUM_IOS-1 downto 0);
        
        lock_in : in std_logic                        
    );
end afswitch;

architecture Behavioral of afswitch is
component IOBUF is
port (
    I : in std_logic;
    O : out std_logic;
    T : in std_logic;
    IO : inout std_logic
);
end component;
signal i_io_o : std_logic_vector(NUM_IOS-1 downto 0);
signal i_io_t : std_logic_vector(NUM_IOS-1 downto 0);
signal i_io_i : std_logic_vector(NUM_IOS-1 downto 0);

signal i_p_i : std_logic_vector(NUM_IOS-1 downto 0);
signal i_p_o : std_logic_vector(NUM_IOS-1 downto 0);
signal i_p_t : std_logic_vector(NUM_IOS-1 downto 0);


begin
    spi0_io0_i <= i_p_i(0);
    i_p_o(0) <= spi0_io0_o;
    i_p_t(0) <= spi0_io0_t;

    spi0_io1_i <= i_p_i(1);
    i_p_o(1) <= spi0_io1_o;
    i_p_t(1) <= spi0_io1_t;

    spi0_sck_i <= i_p_i(2);
    i_p_o(2) <= spi0_sck_o;
    i_p_t(2) <= spi0_sck_t;
    
    spi0_ss_i(0) <= '1' when af_o(3) = '0' else i_p_i(3);
    i_p_o(3) <= spi0_ss_o(0);
    i_p_t(3) <= spi0_ss_t;

    spi0_ss_i(1) <= '1' when af_o(4) = '0' else i_p_i(4);
    i_p_o(4) <= spi0_ss_o(1);
    i_p_t(4) <= spi0_ss_t;

-- I2C0
    i2c0_scl_i <= '1' when af_o(8) = '0' else i_p_i(5); -- default read high if not selected
    i_p_o(5) <= i2c0_scl_o;
    i_p_t(5) <= i2c0_scl_t;
    
    i2c0_sda_i <= '1' when af_o(6) = '0' else i_p_i(6); -- default read high if not selected
    i_p_o(6) <= i2c0_sda_o;
    i_p_t(6) <= i2c0_sda_t;
-- UART0
    rx <= '1' when af_o(7) = '0' else i_p_i(7);   -- default read high if not selected
    i_p_o(7) <= '0';
    i_p_t(7) <= '1';   -- always input
    
--    i_p_i(11) not used for uart
    i_p_o(8) <= tx;
    i_p_t(8) <= '0';   -- always output
    
gen_mux: for I in 0 to NUM_IOS-1 generate
-- select output and enable
    i_io_o(I) <= gpio_io_o(I) when af_o(I) = '0' else i_p_o(I);
    i_io_t(I) <= gpio_io_t(I) when af_o(I) = '0' else i_p_t(I);
    
-- mux on io pin with tristate
    io(I) <= 'Z' when i_io_t(I) = '1' else i_io_o(I);
    
-- read back io pin    
    i_io_I(I) <= io(I);
    gpio_io_i(I) <= io(I);
    i_p_i(I) <= io(I);
end generate;    

gpio_io_i(31) <= lock_in; 

--gen_buf: for I in 0 to NUM_IOS-1 generate
--    afswitch_buf: IOBUF port map (
--    i => i_io_o(I),
--    o => open, --i_io_i(I),
--    t => i_io_t(I),
--    io => io(I)
--);
--end generate;

end Behavioral;



