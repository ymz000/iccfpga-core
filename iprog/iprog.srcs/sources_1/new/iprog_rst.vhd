 -- IOTA Crypto Core
 --
 -- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
 -- discord: pmaxuw#8292
 -- https://gitlab.com/iccfpga-rv
 --
 -- Permission is hereby granted, free of charge, to any person obtaining
 -- a copy of this software and associated documentation files (the
 -- "Software"), to deal in the Software without restriction, including
 -- without limitation the rights to use, copy, modify, merge, publish,
 -- distribute, sublicense, and/or sell copies of the Software, and to
 -- permit persons to whom the Software is furnished to do so, subject to
 -- the following conditions:
 --
 -- The above copyright notice and this permission notice shall be
 -- included in all copies or substantial portions of the Software.
 --
 -- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 -- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 -- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 -- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 -- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 -- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 -- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR


-- base-code was found here: 
-- https://forums.xilinx.com/t5/FPGA-Configuration/Artix-7-ICAPE2-and-STARTUPE2/td-p/624804


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.

library UNISIM;
use UNISIM.VComponents.all;

entity iprog_rst is
--  generic (
--    WBSTAR  : std_logic_vector(31 downto 0) := x"00000000"     -- insert your warm boot address here
--  );
  port (
    clk : in  std_logic;
    reset : in  std_logic;
    iprog : in std_logic;
    addr : in std_logic_vector(15 downto 0);
    rip : out std_logic
  );
end iprog_rst;

architecture Behavioral of iprog_rst is
constant CMAX : integer := 32;
constant WAITTIME : integer := 100000000;   -- 1 second

signal cs_l : std_logic := '1';
signal r_wx : std_logic := '1';

signal data : std_logic_vector(31 downto 0);
signal d_swapped : std_logic_vector(31 downto 0);
 
begin
 
process(CLK)
    variable state : integer range 0 to 7;
    variable count : integer range 0 to CMAX := 0;
    variable waitcnt : integer range 0 to WAITTIME := 0;
begin
    if rising_edge(clk) then
        if reset='0' then
            state := 0;
        else 
            case state is
                when 0 =>
                    rip  <= '0';
                    cs_l <= '1';
                    r_wx <= '1';
                    count := 0;
                    waitcnt := WAITTIME;
                    data <= (others => '1');
                    if iprog='1' then
                        state := 1;
                    end if;
                when 1 =>
                    if iprog='0' then
                        state := 0;
                    end if;
                    waitcnt := waitcnt - 1;
                    -- wait a second
                    if waitcnt = 0 then
                        state := 2;
                    end if;
                when 2 =>
                    rip <= '1';
                    case count is
                        when  0 => data <= x"FFFFFFFF";   -- Dummy Word
                        when  1 => data <= x"FFFFFFFF";   -- Dummy Word
                        when  2 => data <= x"FFFFFFFF";   -- Dummy Word
                        when  3 => data <= x"FFFFFFFF";   -- Dummy Word
                        when  4 => data <= x"FFFFFFFF";   -- Dummy Word
                        when  5 => data <= x"20000000";   -- Type 1 NO OP
                           cs_l  <= '0';
                           r_wx  <= '0';
                        when  6 => data <= x"AA995566";   -- Sync Word
                        when  7 => data <= x"20000000";   -- Type 1 NO OP
                        when  8 => data <= x"20000000";   -- Type 1 NO OP
                        when  9 => data <= x"30020001";   -- Type 1 Write 1 Word to WBSTAR
                        when 10 => data <= addr & x"0000";-- Warm Boot Start Address
                        when 11 => data <= x"20000000";   -- Type 1 NO OP
                        when 12 => data <= x"20000000";   -- Type 1 NO OP
                        when 13 => data <= x"30008001";   -- Type 1 Write 1 Words to CMD
                        when 14 => data <= x"0000000F";   -- IPROG Command
                        when 15 => data <= x"20000000";   -- Type 1 NO OP
                        when 16 => data <= x"20000000";   -- Type 1 NO OP
                           cs_l     <= '1';
                           r_wx     <= '1';
                        when others =>
                           cs_l     <= '1';
                           r_wx     <= '1';
                    end case;

                    if count /= CMAX then
                        count := count + 1;
                    end if;               
                when others =>
                    state := 0;
            end case;
        end if;
    end if;
end process;
 
loop0: for J in 0 to 3 generate
    loop1: for I in 0 to 7 generate
        d_swapped(J*8+7-I) <= data(J*8+I);
    end generate;
end generate;
 
 
ICAPE2_inst: ICAPE2
generic map (
   ICAP_WIDTH => "X32"  -- Specifies the input and output data width.
)
port map (
   O     => open,       -- 32-bit output: Configuration data output bus
   CLK   => CLK,        -- 1-bit input: Clock Input
   CSIB  => cs_l,       -- 1-bit input: Active-Low ICAP Enable
   I     => d_swapped,  -- 32-bit input: Configuration data input bus
   RDWRB => r_wx        -- 1-bit input: Read/Write Select input
);
 
end Behavioral;
